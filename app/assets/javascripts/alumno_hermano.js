$(document).ready(function() {
  $('#content-table-search-alumnos').css('display', 'none');
  $('#form_comportamiento_alumno_hermano').validate({
    rules: {
      'alumno_nombre_completo': {
        required: true
      },
      'alumno_comportamiento[comportamiento_id]': {
        required: true
      },
      'alumno_comportamiento[nota]': {
        required: true
      },
      'alumno_comportamiento[comportamiento_id]': {
        required: true
      }
    },
    submitHandler: function(form) {
      return form.submit();
    }
  });
  $('#form-search-alumno-hermano').validate({
    rules: {
      matricula: {
        required: true
      }
    },
    submitHandler: function(form) {
      var button, text;
      button = void 0;
      text = void 0;
      button = $('#btn');
      text = button.val();
      $(form).ajaxSubmit({
        beforeSend: function() {
          button.val('Search...').prop('disabled', true);
        },
        error: function() {},
        success: function(response) {
          //var tr;
          $('#content-table-search-alumnos').show();
          if (response.length > 0) {
            $('#table-search-alumnos tbody').html(response);
          } else {
            $('#table-search-alumnos tbody').html('<tr><td colspan="3">No se encontraron datos</td></tr>');
          }
        },
        complete: function() {
          return button.val(text).prop('disabled', false);
        },
        dataType: 'HTML'
      });
    }
  });
  $('#content-table-search-alumnos').delegate('#select-search-alumno', 'click', function() {

    var alumno_id = $(this).find('#alumno_id').val();
    var alumno_nombre_completo = $(this).find('#alumno_nombre_completo').val();
    var alumno_matricula = $(this).find('#alumno_matricula').val();
    
    if (alumno_id) {
      $('#input_alumno_nombre_completo').val(alumno_nombre_completo);
      $('#input_alumno_hermano_id').val(alumno_id);
      $('#input_alumno_matricula').val(alumno_matricula);
    }

  });
});