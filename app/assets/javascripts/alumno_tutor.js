$(document).ready(function() {
  $('#form-alumno-tutor-alumno').validate({
    rules: {
      'nombre_completo': {
        required: true
      },
      'fecha_nacimiento': {
        required: true
      },
      'alumno_tutor[alumno_id]': {
        required: true
      },
      'alumno_tutor[tutor_id]': {
        required: true
      }
    },
    messages: {
      'nombre_completo': {
        required: 'Campo requerido'
      },
      'fecha_nacimiento': {
        required: 'Campo requerido'
      },
      'alumno_tutor[alumno_id]': {
        required: 'Campo requerido'
      },
      'alumno_tutor[tutor_id]': {
        required: 'Campo requerido'
      }
    },
    submitHandler: function(form) {
      return form.submit();
    }
  });
  
  $('.new_alumno_tutor, .edit_alumno_tutor').validate({
    rules: {
      'alumno_tutor[nombre]': {
        required: true
      },
      'alumno_tutor[apellido_paterno]': {
        required: true
      },
      'alumno_tutor[apellido_materno]': {
        required: true
      },
      'alumno_tutor[parentesco]': {
        required: true
      },
      'alumno_tutor[sexo]': {
        required: true
      },
      'alumno_tutor[escolaridad_id]': {
        required: true
      },
      'alumno_tutor[ocupacion_id]': {
        required: true
      }
    },
    messages: {
      'alumno_tutor[nombre]': {
        required: 'Campo requerido'
      },
      'alumno_tutor[apellido_paterno]': {
        required: 'Campo requerido'
      },
      'alumno_tutor[apellido_materno]': {
        required: 'Campo requerido'
      },
      'alumno_tutor[parentesco]': {
        required: 'Campo requerido'
      },
      'alumno_tutor[sexo]': {
        required: 'Campo requerido'
      },
      'alumno_tutor[escolaridad_id]': {
        required: 'Campo requerido'
      },
      'alumno_tutor[ocupacion_id]': {
        required: 'Campo requerido'
      }
    },
    submitHandler: function(form) {
      return form.submit();
    }
  });
});

$('#content-table-search-tutors').css('display', 'none');

$('#form-search-tutor').validate({
  rules: {
    nombre: {
      required: true
    },
    apellido_paterno: {
      required: true
    }
  },
  messages: {
    'nombre': {
      required: 'Campo requerido'
    },
    'apellido_paterno': {
      required: 'Campo requerido'
    }
  },
  submitHandler: function(form) {
    var button, text;
    button  = 0;
    text    = 0;
    button  = $('#btn');
    text = button.val();
    $(form).ajaxSubmit({
      beforeSend: function() {
        button.val('Search...').prop('disabled', true);
      },
      error: function() {

      },
      success: function(response) {
        $('#content-table-search-tutors').show();
        if (response.length > 0) {
          $('#table-search-tutors tbody').html(response);
        } else {
          $('#table-search-tutors tbody').html('<tr><td colspan="3">No se encontraron datos</td></tr>');
        }
      },
      complete: function() {
        return button.val(text).prop('disabled', false);
      },
      dataType: 'HTML'
    });
  }
});

$('#content-table-search-tutors').delegate('#select-search-tutor', 'click', function() {
  
  var tutor_id                = $(this).find('#tutor_id').val();
  var tutor_nombre_completo   = $(this).find('#tutor_nombre_completo').val();
  var tutor_fecha_nacimiento  = $(this).find('#tutor_fecha_nacimiento').val();
  var tutor_lugar_nacimiento  = $(this).find('#tutor_lugar_nacimiento').val();

  if (tutor_id) {
    $('#alumno_tutor_tutor_id').val(tutor_id);

    $('#input_tutor_nombre_completo').val(tutor_nombre_completo);
    $('#input_tutor_fecha_nacimiento').val(tutor_fecha_nacimiento);
    $('#input_tutor_lugar_nacimiento').val(tutor_lugar_nacimiento);
  }
});