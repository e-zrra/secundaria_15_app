$(document).ready ->
  $('.new_alumno, .edit_alumno').validate
    rules:
      'alumno[nombre]': required: true
      'alumno[apellido_paterno]': required: true
      'alumno[apellido_materno]': required: true
      'alumno[curp]': required: true
      'alumno[grado_grupo_id]': required: true
      'alumno[promedio]' : maxlength: 4
    messages:
      'alumno[nombre]':
        required: 'Campo requerido'
      'alumno[apellido_paterno]':
        required: 'Campo requerido'
      'alumno[apellido_materno]':
        required: 'Campo requerido'
      'alumno[curp]':
        required: 'Campo requerido'
       'alumno[grado_grupo_id]':
        required: 'Campo requerido'
    submitHandler: (form) ->
      form.submit()