$('.dropdown-toggle').dropdown();

$(function () {
    $('.show-data').on('click', function(event)
    {
        var href = $(this).attr('href');

        if (href) {
        	event.stopPropagation();
            window.location.href = href;
        };
    })
});

$(function () {
    //Delete A record of any model
    $('.delete-data').on('click', function(event) {
        event.stopPropagation();
        event.preventDefault();
        var anchor = $(this);
        confirmBox('Eliminar registro', '¿Estas seguro en eliminar el registro?', function() {
            destroyRecord(anchor);
        });
    });
});

// delete a record from the database
function destroyRecord(anchor) {

    var form = anchor.closest('form');
    var text = anchor.text();

    form.submit();

    /*
    form.ajaxSubmit({
        beforeSend: function() {
            anchor.text('Deleting...').prop('disabled', true);
        },
        error: function() {
            showDialog('ajax.error');
        },
        success: function(response) {
            if (response.msg) alertBox('success', 'Información', response.msg, 'Ok');
            //location.reload();
        },
        complete: function() {
            anchor.text(text).prop('disabled', false);
        }
    });*/
}

// bootbox: alert dialog
function alertBox(type, header, content, buttonText) {
    if( ! buttonText) {
        buttonText = 'Ok';
    }
    bootbox.dialog({
        className: 'my-custom-dialog ' + type + '-dialog',
        message: content,
        title: header,
        buttons: {
            main: {
                className: 'btn-' + type + ' btn-sm',
                label: buttonText
            }
        }
    });
}

// bootbox: confirm dialog
function confirmBox(header, content, callback) {
    bootbox.dialog({
        className: 'my-custom-dialog warning-dialog',
        message: content,
        title: header,
        buttons: {
            cancel: {
                className: 'btn-default btn-sm',
                label: 'No'
            },
            main: {
                className: 'btn-warning btn-sm',
                label: 'Si',
                callback: callback
            }
        }
    });
}

function str_limit (text, limit, str_end) {
    var string = '';
    if (text.length > limit) {
        var str = text.substring(1, limit);
        string = str + ' ' + str_end;
    } else {
        string = text;
    }
    return string;
}