$(document).ready(function() {
  $('#form-profile').validate({
    rules: {
      'user[email]': {
        required: true,
        email: true
      },
      'user[first_name]': {
        required: true
      },
      'user[password]':{
      	minlength: 8
      },
      'user[password_confirmation]':{
      	minlength: 8,
      	equalTo: "#user_password"
      }
    },
    messages: {
      'user[email]': {
        required: 'Campo requerido',
        email: "No es correo electrónico"
      },
      'user[first_name]': {
        required: 'Campo requerido'
      },
      'user[password]':{
      	minlength: "Sebe de tener 8 caracteres minimo"
      },
      'user[password_confirmation]':{
      	minlength: "Sebe de tener 8 caracteres minimo",
      	equalTo: "No es la misma contraseña"
      }
    },
    submitHandler: function(form) {
      return form.submit();
    }
  });

});