class AlumnoCiclosController < ApplicationController
  
  include ApplicationHelper
  before_action :authenticate_user!
  before_action :set_alumno_ciclo, only: [:show, :edit, :update, :destroy]


  # DELETE /grado_grupos/1
  # DELETE /grado_grupos/1.json
  def destroy
    @alumno_ciclo.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Ciclo fue eliminado.' }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_alumno_ciclo
      @alumno_ciclo = AlumnoCiclo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def alumno_ciclo_params
      params.require(:alumno_ciclo).permit(:grado, :grupo)
    end
end
