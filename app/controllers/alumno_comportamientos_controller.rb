class AlumnoComportamientosController < ApplicationController
  
  include ApplicationHelper
  before_action :authenticate_user!
  before_action :search, only: [:show, :edit, :new]
  Root_reportes_comportamiento_directory = Rails.root + 'app/assets/images/reportes_comportamientos'
  before_action :set_alumno_comportamiento, only: [:show, :edit, :update, :destroy]

  # GET /alumno_comportamientos
  # GET /alumno_comportamientos.json
  def index
    if params['search']
      search  = params['search']
      @alumno_comportamientos = AlumnoComportamiento.joins(:alumno).where("alumnos.nombre_completo ILIKE ? OR alumnos.matricula ILIKE ?","%#{search}%", "%#{search}%").paginate(:page => params[:page], :per_page => 8)
    else
      @alumno_comportamientos = AlumnoComportamiento.paginate(:page => params[:page], :per_page => 8)
    end
  end

  # GET /alumno_comportamientos/1
  # GET /alumno_comportamientos/1.json
  def show
  end

  # GET /alumno_comportamientos/new
  def new
    @alumno_comportamiento = AlumnoComportamiento.new
  end

  # GET /alumno_comportamientos/1/edit
  def edit
  end

  # POST /alumno_comportamientos
  # POST /alumno_comportamientos.json
  def create
    @alumno_comportamiento = AlumnoComportamiento.new(alumno_comportamiento_params)
    if params[:file]
      @alumno_comportamiento.fotografia = upload_file_helper(params[:file], Root_reportes_comportamiento_directory)
    end
    respond_to do |format|
      if @alumno_comportamiento.save
        format.html { redirect_to @alumno_comportamiento, notice: 'Reporte fue creado.' }
        format.json { render :show, status: :created, location: @alumno_comportamiento }
      else
        format.html { render :new }
        format.json { render json: @alumno_comportamiento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /alumno_comportamientos/1
  # PATCH/PUT /alumno_comportamientos/1.json
  def update
    if params[:file]
      @alumno_comportamiento.fotografia = upload_file_helper(params[:file], Root_reportes_comportamiento_directory)
    end
    respond_to do |format|
      if @alumno_comportamiento.update(alumno_comportamiento_params)
        format.html { redirect_to @alumno_comportamiento, notice: 'Reporte fue actualizado.' }
        format.json { render :show, status: :ok, location: @alumno_comportamiento }
      else
        format.html { render :edit }
        format.json { render json: @alumno_comportamiento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /alumno_comportamientos/1
  # DELETE /alumno_comportamientos/1.json
  def destroy
    #AlumnoComportamiento.destroy_all
    @alumno_comportamiento.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Reporte fue eliminado.' }
      format.json { head :no_content }
    end
  end #alumno_comportamientos_url

  # GET /alumnos/search_alumno
  def search_alumno
    matricula         = params[:matricula] == nil ? '' : params[:matricula]
    apellido_paterno  = params[:apellido_paterno] ==   nil ? '' : params[:apellido_paterno]
    @alumnos          = Alumno.where("matricula ILIKE '%#{matricula}%'").where("apellido_paterno ILIKE '%#{apellido_paterno}%'")
    render "alumno_comportamientos/_search_alumno", :layout => false
  end

  private

    def search
      redirect_to_search_helper(alumno_comportamientos_url)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_alumno_comportamiento
      @alumno_comportamiento = AlumnoComportamiento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def alumno_comportamiento_params
      params.require(:alumno_comportamiento).permit(:alumno_id, :comportamiento_id, :fecha, :nota, :fotografia)
    end
end
