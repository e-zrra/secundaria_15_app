class AlumnoHermanosController < ApplicationController
  include ApplicationHelper
  before_action :search, only: [:show, :edit, :new]
  before_action :set_alumno_hermano, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /alumno_hermanos
  # GET /alumno_hermanos.json
  def index
    if params['search']
      search  = params['search']
      @alumno_hermanos = AlumnoHermano.joins(:hermano).where("nombre_completo ILIKE ? OR matricula ILIKE ?","%#{search}%", "%#{search}%").paginate(:page => params[:page], :per_page => 8)
    else
      @alumno_hermanos = AlumnoHermano.paginate(:page => params[:page], :per_page => 10)
    end
  end

  # GET /alumno_hermanos/1
  # GET /alumno_hermanos/1.json
  def show
  end

  # GET /alumno_hermanos/new
  def new
    @alumno = Alumno.find(params[:alumno_id])
    @alumno_hermano = AlumnoHermano.new
  end

  # GET /alumno_hermanos/1/edit
  def edit
    @alumno = Alumno.find(params[:alumno_id])
  end

  # POST /alumno_hermanos
  # POST /alumno_hermanos.json
  def create
    @alumno_hermano = AlumnoHermano.new(alumno_hermano_params)

    respond_to do |format|
      if @alumno_hermano.save
        format.html { redirect_to @alumno_hermano, notice: 'Registro de hermano fue creado.' }
        format.json { render :show, status: :created, location: @alumno_hermano }
      else
        format.html { render :new }
        format.json { render json: @alumno_hermano.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /alumno_hermanos/1
  # PATCH/PUT /alumno_hermanos/1.json
  def update
    respond_to do |format|
      if @alumno_hermano.update(alumno_hermano_params)
        format.html { redirect_to @alumno_hermano, notice: 'Registro de hermano fue actualizado.' }
        format.json { render :show, status: :ok, location: @alumno_hermano }
      else
        format.html { render :edit }
        format.json { render json: @alumno_hermano.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /alumno_hermanos/1
  # DELETE /alumno_hermanos/1.json
  def destroy
    @alumno_hermano.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Registro de hermano fue eliminado.' }
      format.json { head :no_content }
    end
  end

  def search_alumno
    matricula         = params[:matricula] == nil ? '' : params[:matricula]
    apellido_paterno  = params[:apellido_paterno] ==   nil ? '' : params[:apellido_paterno]
    @alumnos          = Alumno.where("matricula ILIKE '%#{matricula}%'").where("apellido_paterno ILIKE '%#{apellido_paterno}%'")
    render "alumno_hermanos/_search_alumno", :layout => false
  end

  private

    def search
      redirect_to_search_helper(alumno_hermanos_url)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_alumno_hermano
      @alumno_hermano = AlumnoHermano.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def alumno_hermano_params
      params.require(:alumno_hermano).permit(:alumno_id, :hermano_id)
    end
end
