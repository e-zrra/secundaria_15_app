class AlumnoTutorsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_alumno_tutor, only: [:show, :edit, :update, :destroy]

  # GET /alumno_tutors
  # GET /alumno_tutors.json
  def index
    @alumno_tutors = AlumnoTutor.all
  end

  # GET /alumno_tutors/1
  # GET /alumno_tutors/1.json
  def show
  end

  # GET /alumno_tutors/new
  def new
    @alumno = Alumno.find(params[:alumno_id])
    @alumno_tutor = AlumnoTutor.new
    @tutor = Tutor.new
  end

  # GET /alumno_tutors/1/edit
  def edit
    @alumno = Alumno.find(params[:alumno_id])
    @tutor = Tutor.find(@alumno_tutor.tutor.id)
  end

  # POST /alumno_tutors
  # POST /alumno_tutors.json
  def create
    @alumno_tutor = AlumnoTutor.new(alumno_tutor_params)

    respond_to do |format|
      if @alumno_tutor.save
        format.html { redirect_to @alumno_tutor, notice: 'Tutor fue asignado.' }
        format.json { render :show, status: :created, location: @alumno_tutor }
      else
        format.html { render :new }
        format.json { render json: @alumno_tutor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /alumno_tutors/1
  # PATCH/PUT /alumno_tutors/1.json
  def update
    respond_to do |format|
      if @alumno_tutor.update(alumno_tutor_params)
        format.html { redirect_to @alumno_tutor, notice: 'Alumno y tutor fue modificado.' }
        format.json { render :show, status: :ok, location: @alumno_tutor }
      else
        format.html { render :edit }
        format.json { render json: @alumno_tutor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /alumno_tutors/1
  # DELETE /alumno_tutors/1.json
  def destroy
    @alumno_tutor.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Tutor fue removido correctamente.' }
      format.json { head :no_content }
    end
  end

  # GET /alumno_tutors/search_alumno
  def search_tutor
    nombre            = params[:nombre] == nil ? '' : params[:nombre]
    apellido_paterno  = params[:apellido_paterno] ==   nil ? '' : params[:apellido_paterno]
    @tutors           = Tutor.where("nombre ILIKE '%#{nombre}%'").where("apellido_paterno ILIKE '%#{apellido_paterno}%'")
    #format.html { render "alumno_tutors/_search_tutor", :layout => false  } 
    render "alumno_tutors/_search_tutor", :layout => false
  end 

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_alumno_tutor
      @alumno_tutor = AlumnoTutor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def alumno_tutor_params
      params.require(:alumno_tutor).permit(:alumno_id, :tutor_id)
    end
end
