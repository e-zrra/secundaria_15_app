class AlumnosController < ApplicationController
  include ApplicationHelper, AlumnosHelper
  before_action :authenticate_user!
  before_action :search, only: [:show, :edit, :new, :alumnos_by_group_grado, :increase_by_grupo_grado, :increase_by_ciclo_escolar]
  before_action :set_alumno, only: [:show, :edit, :update, :destroy]
  Root_alumnos_directory = Rails.root + 'app/assets/images/alumnos'

  # GET /alumnos
  # GET /alumnos.json
  def index
    if params['search']
      search  = params['search']
      @alumnos = Alumno.where("nombre_completo ILIKE ? OR matricula ILIKE ?","%#{search}%", "%#{search}%").paginate(:page => params[:page], :per_page => 8)
    else
      @alumnos = Alumno.paginate(:page => params[:page], :per_page => 8)
    end
  end

  # GET /alumnos/1
  # GET /alumnos/1.json
  def show
    @tutores  = Tutor.all
    @hermanos = AlumnoHermano.where(:alumno_id => @alumno.id)
  end

  # GET /alumnos/new
  def new
    @alumno = Alumno.new
  end

  # GET /alumnos/1/edit
  def edit
    @tutores = AlumnoTutor.where(:alumno_id => @alumno.id)
  end

  # POST /alumnos
  # POST /alumnos.json
  def create
    @alumno                 = Alumno.new(alumno_params)
    @alumno.matricula       = get_matricula()
    @alumno.nombre_completo = get_full_name()

    if params[:file]
      @alumno.fotografia    = upload_file_helper(params[:file], Root_alumnos_directory)
    end

    respond_to do |format|
      if @alumno.save
        if @alumno.ciclo_escolar_id
          set_ciclo_escolar()
        end
        
        format.html { redirect_to @alumno, notice: 'Alumno fue creado.' }
        format.json { render :show, status: :created, location: @alumno }
      else
        format.html { render :new }
        format.json { render json: @alumno.errors, status: :unprocessable_entity }
      end
    end
  end

  def check_ciclo
    if @alumno.ciclo_escolar_id.to_i != params[:alumno][:ciclo_escolar_id].to_i
      return true
    end
  end

  # PATCH/PUT /alumnos/1
  # PATCH/PUT /alumnos/1.json
  def update
    if params[:file]
      @alumno.fotografia = upload_file_helper(params[:file], Root_alumnos_directory)
    end
    
    @alumno.nombre_completo = get_full_name()
    
    if check_ciclo() == true
      if params[:alumno][:ciclo_escolar_id]
        set_ciclo_escolar()
      end
    end

    respond_to do |format|
      if @alumno.update(alumno_params)
        format.html { redirect_to @alumno, notice: 'Alumno fue actualizado.' }
        format.json { render :show, status: :ok, location: @alumno }
      else
        format.html { render :edit }
        format.json { render json: @alumno.errors, status: :unprocessable_entity }
      end
    end
  end

  def get_full_name
    return "#{params[:alumno][:nombre]} #{params[:alumno][:apellido_paterno]} #{params[:alumno][:apellido_materno]}"
  end

  def set_ciclo_escolar
    ciclo_escolar = params[:alumno][:ciclo_escolar_id]
    if ciclo_escolar != nil or ciclo_escolar != ''
      AlumnoCiclo.create(:ciclo_escolar_id => ciclo_escolar,:alumno_id => @alumno.id)
    end
  end

  # DELETE /alumnos/1
  # DELETE /alumnos/1.json
  def destroy
    AlumnoHermano.where(alumno_id: @alumno.id).destroy_all
    AlumnoTutor.where(alumno_id: @alumno.id).destroy_all
    AlumnoCiclo.where(alumno_id: @alumno.id).destroy_all

    if @alumno.fotografia
      remove_file_helper(@alumno.fotografia, Root_alumnos_directory)
    end

    @alumno.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Alumno fue eliminado.' }
      format.json { head :no_content }
    end
  end

  # GET /alumnos/search_alumno
  def search_alumno
    matricula         = params[:matricula] == nil ? '' : params[:matricula]
    apellido_paterno  = params[:apellido_paterno] ==   nil ? '' : params[:apellido_paterno]
    @alumnos          = Alumno.where("matricula ILIKE '%#{matricula}%'").where("apellido_paterno ILIKE '%#{apellido_paterno}%'")
    render json: @alumnos
  end

   # GET /alumnos/search_alumno
  def alumnos_by_group_grado
    if params['grado_grupo'] or params['grado_grupo'] or params['ciclo_escolar']
      turno       = params['turno']
      grado_grupo = params['grado_grupo']
      ciclo_escolar = params['ciclo_escolar']

      @alumnos = Alumno.where(grado_grupo_id: grado_grupo).where(turno: turno).where(ciclo_escolar: ciclo_escolar)
    else
      @alumnos = Alumno.order(created_at: :desc).take(20)
    end
  end

  def increase_by_grupo_grado
    if params['grado_grupo'] or params['grado_grupo'] or params['ciclo_escolar']
      turno       = params['turno']
      grado_grupo = params['grado_grupo']
      ciclo_escolar = params['ciclo_escolar']
      @alumnos = Alumno.where(grado_grupo_id: grado_grupo).where(turno: turno).where(ciclo_escolar: ciclo_escolar)
    else
      @alumnos = Alumno.order(created_at: :desc).take(0)
    end

    if params['update_grado_grupo'] and @alumnos
      update_grado_grupo = params['update_grado_grupo']

      @alumnos.each do |alumno|
        @alumno = Alumno.find(alumno.id)
        @alumno.grado_grupo_id = update_grado_grupo
        @alumno.save
      end

      @alumnos = Alumno.where(grado_grupo_id: update_grado_grupo).where(turno: turno).where(ciclo_escolar: ciclo_escolar)
      flash.now[:notice] = "Se actualizado correctamente el grado y grupo"
    end
  end

  def increase_by_ciclo_escolar
    if params['grado_grupo'] or params['grado_grupo'] or params['ciclo_escolar']
      turno         = params['turno']
      grado_grupo   = params['grado_grupo']
      ciclo_escolar = params['ciclo_escolar']
      @alumnos = Alumno.where(grado_grupo_id: grado_grupo).where(turno: turno).where(ciclo_escolar: ciclo_escolar)
    else
      @alumnos = Alumno.order(created_at: :desc).take(0)
    end

    if params['update_ciclo_escolar'] and @alumnos
      update_ciclo_escolar = params['update_ciclo_escolar']

      @alumnos.each do |alumno|
        

        @alumno = Alumno.find(alumno.id)
        if @alumno.ciclo_escolar_id.to_i != update_ciclo_escolar.to_i
          AlumnoCiclo.create(:ciclo_escolar_id => update_ciclo_escolar,:alumno_id => @alumno.id)
        end
        @alumno.ciclo_escolar_id = update_ciclo_escolar
        @alumno.save



      end

      @alumnos = Alumno.where(ciclo_escolar_id: update_ciclo_escolar).where(turno: turno).where(grado_grupo_id: grado_grupo)
      flash.now[:notice] = "Se actualizado correctamente el ciclo escolar"
    end
  end

  private

    def search
      redirect_to_search_helper(alumnos_url)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_alumno
      @alumno = Alumno.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def alumno_params
      params.require(:alumno).permit(:nombre, :apellido_paterno, :apellido_materno, :fecha_nacimiento, :lugar_nacimiento, :curp, :escuela_procedencia, :promedio, :toma_medicamento, :antecedentes_medicos, :estado_salud_id, :turno_id,:domicilio, :telefono, :celular, :nota, :grado_grupo_id, :ciclo_escolar_id, :fotografia)
    end
end
