class CicloEscolarsController < ApplicationController
  
  include ApplicationHelper, AlumnosHelper
  before_action :authenticate_user!
  before_action :search, only: [:show, :edit, :new]
  before_action :set_ciclo_escolar, only: [:show, :edit, :update, :destroy]

  # GET /ciclo_escolars
  # GET /ciclo_escolars.json
  def index
    if params['search']
      search  = params['search']
      @ciclo_escolars = CicloEscolar.where("nombre ILIKE ? OR descripcion ILIKE ?","%#{search}%", "%#{search}%").paginate(:page => params[:page], :per_page => 8)
    else
      @ciclo_escolars = CicloEscolar.paginate(:page => params[:page], :per_page => 10)
    end
  end

  # GET /ciclo_escolars/1
  # GET /ciclo_escolars/1.json
  def show
  end

  # GET /ciclo_escolars/new
  def new
    @ciclo_escolar = CicloEscolar.new
  end

  # GET /ciclo_escolars/1/edit
  def edit
  end

  # POST /ciclo_escolars
  # POST /ciclo_escolars.json
  def create
    @ciclo_escolar = CicloEscolar.new(ciclo_escolar_params)

    respond_to do |format|
      if @ciclo_escolar.save
        format.html { redirect_to @ciclo_escolar, notice: 'Ciclo escolar fue creado.' }
        format.json { render :show, status: :created, location: @ciclo_escolar }
      else
        format.html { render :new }
        format.json { render json: @ciclo_escolar.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ciclo_escolars/1
  # PATCH/PUT /ciclo_escolars/1.json
  def update
    respond_to do |format|
      if @ciclo_escolar.update(ciclo_escolar_params)
        format.html { redirect_to @ciclo_escolar, notice: 'Ciclo escolar fue actualizado.' }
        format.json { render :show, status: :ok, location: @ciclo_escolar }
      else
        format.html { render :edit }
        format.json { render json: @ciclo_escolar.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ciclo_escolars/1
  # DELETE /ciclo_escolars/1.json
  def destroy
    @ciclo_escolar.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Ciclo escolar fue eliminado.' }
      format.json { head :no_content }
    end
  end #ciclo_escolars_url

  private

    def search
      redirect_to_search_helper(ciclo_escolars_url)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_ciclo_escolar
      @ciclo_escolar = CicloEscolar.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ciclo_escolar_params
      params.require(:ciclo_escolar).permit(:nombre, :descripcion, :inicio_fecha, :finaliza_fecha)
    end
end
