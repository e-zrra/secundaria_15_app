class ComportamientosController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!
  before_action :search, only: [:show, :edit, :new]
  before_action :set_comportamiento, only: [:show, :edit, :update, :destroy]
  # GET /comportamientos
  # GET /comportamientos.json
  def index
    if params['search']
      search  = params['search']
      @comportamientos = Comportamiento.where("codigo ILIKE ? OR nombre ILIKE ? OR descripcion ILIKE ?","%#{search}%", "%#{search}%", "%#{search}%").paginate(:page => params[:page], :per_page => 8)
    else
      @comportamientos = Comportamiento.paginate(:page => params[:page], :per_page => 10)
    end
  end

  # GET /comportamientos/1
  # GET /comportamientos/1.json
  def show
  end

  # GET /comportamientos/new
  def new
    @comportamiento = Comportamiento.new
  end

  # GET /comportamientos/1/edit
  def edit
  end

  # POST /comportamientos
  # POST /comportamientos.json
  def create
    @comportamiento = Comportamiento.new(comportamiento_params)
    respond_to do |format|
      if @comportamiento.save
        format.html { redirect_to @comportamiento, notice: 'Comportamiento fue creado.' }
        format.json { render :show, status: :created, location: @comportamiento }
      else
        format.html { render :new }
        format.json { render json: @comportamiento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comportamientos/1
  # PATCH/PUT /comportamientos/1.json
  def update
    respond_to do |format|
      if @comportamiento.update(comportamiento_params)
        format.html { redirect_to @comportamiento, notice: 'Comportamiento fue actualizado.' }
        format.json { render :show, status: :ok, location: @comportamiento }
      else
        format.html { render :edit }
        format.json { render json: @comportamiento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comportamientos/1
  # DELETE /comportamientos/1.json
  def destroy
    @comportamiento.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Comportamiento fue eleminado.' }
      format.json { head :no_content }
    end
  end #comportamientos_url

  private

    def search
      redirect_to_search_helper(alumnos_url)
    end
    
    # Use callbacks to share common setup or constraints between actions.
    def set_comportamiento
      @comportamiento = Comportamiento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comportamiento_params
      params.require(:comportamiento).permit(:codigo, :nombre, :descripcion)
    end
end
