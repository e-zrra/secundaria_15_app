class DocentesController < ApplicationController

  include ApplicationHelper, AlumnosHelper
  before_action :authenticate_user!
  before_action :search, only: [:show, :edit, :new]
  before_action :set_docente, only: [:show, :edit, :update, :destroy]
  Root_docentes_directory = Rails.root + 'app/assets/images/docentes'
  # GET /docentes
  # GET /docentes.json
  def index
    if params['search']
      search  = params['search']
      @docentes = Docente.where("nombre_completo ILIKE ? OR telefono ILIKE ?","%#{search}%", "%#{search}%").paginate(:page => params[:page], :per_page => 8)
    else
      @docentes = Docente.paginate(:page => params[:page], :per_page => 10)
    end
  end

  # GET /docentes/1
  # GET /docentes/1.json
  def show
  end

  # GET /docentes/new
  def new
    @docente = Docente.new
  end

  # GET /docentes/1/edit
  def edit
  end

  # POST /docentes
  # POST /docentes.json
  def create
    @docente = Docente.new(docente_params)
    @docente.nombre_completo = get_full_name()
    if params[:file]
      @docente.fotografia = upload_file_helper(params[:file], Root_docentes_directory)
    end
    respond_to do |format|
      if @docente.save
        format.html { redirect_to @docente, notice: 'Docente fue creado.' }
        format.json { render :show, status: :created, location: @docente }
      else
        format.html { render :new }
        format.json { render json: @docente.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /docentes/1
  # PATCH/PUT /docentes/1.json
  def update
    @docente.nombre_completo = get_full_name()
    if params[:file]
      @docente.fotografia = upload_file_helper(params[:file], Root_docentes_directory)
    end
    respond_to do |format|
      if @docente.update(docente_params)
        format.html { redirect_to @docente, notice: 'Docente fue actualizado.' }
        format.json { render :show, status: :ok, location: @docente }
      else
        format.html { render :edit }
        format.json { render json: @docente.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /docentes/1
  # DELETE /docentes/1.json
  def destroy
    @docente.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Docente fue eliminado.' }
      format.json { head :no_content }
    end
  end #docentes_url

  private
    
    def search
      redirect_to_search_helper(alumnos_url)
    end

    def get_full_name
      return "#{params[:docente][:nombre]} #{params[:docente][:apellido_paterno]} #{params[:docente][:apellido_materno]}"
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_docente
      @docente = Docente.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def docente_params
      params.require(:docente).permit(:nombre, :apellido_paterno, :apellido_materno, :fecha_nacimiento, :lugar_nacimiento, :telefono, :celular, :domicilio, :fotografia)
    end
end
