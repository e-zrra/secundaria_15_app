class EscolaridadsController < ApplicationController
  
  include ApplicationHelper
  before_action :authenticate_user!
  before_action :search, only: [:show, :edit, :new]
  before_action :set_escolaridad, only: [:show, :edit, :update, :destroy]
  # GET /escolaridads
  # GET /escolaridads.json
  def index
    if params['search']
      search  = params['search']
      @escolaridads = Escolaridad.where("nombre ILIKE ? OR descripcion ILIKE ?","%#{search}%", "%#{search}%").paginate(:page => params[:page], :per_page => 8)
    else
      @escolaridads = Escolaridad.paginate(:page => params[:page], :per_page => 10)
    end
  end

  # GET /escolaridads/1
  # GET /escolaridads/1.json
  def show
  end

  # GET /escolaridads/new
  def new
    @escolaridad = Escolaridad.new
  end

  # GET /escolaridads/1/edit
  def edit
  end

  # POST /escolaridads
  # POST /escolaridads.json
  def create
    @escolaridad = Escolaridad.new(escolaridad_params)

    respond_to do |format|
      if @escolaridad.save
        format.html { redirect_to @escolaridad, notice: 'Escolaridad fue creada.' }
        format.json { render :show, status: :created, location: @escolaridad }
      else
        format.html { render :new }
        format.json { render json: @escolaridad.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /escolaridads/1
  # PATCH/PUT /escolaridads/1.json
  def update
    respond_to do |format|
      if @escolaridad.update(escolaridad_params)
        format.html { redirect_to @escolaridad, notice: 'Escolaridad fue actualizada.' }
        format.json { render :show, status: :ok, location: @escolaridad }
      else
        format.html { render :edit }
        format.json { render json: @escolaridad.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /escolaridads/1
  # DELETE /escolaridads/1.json
  def destroy
    @escolaridad.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Escolaridad fue eliminada.' }
      format.json { head :no_content }
    end
  end #escolaridads_url

  private

    def search
      redirect_to_search_helper(escolaridads_url)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_escolaridad
      @escolaridad = Escolaridad.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def escolaridad_params
      params.require(:escolaridad).permit(:nombre, :descripcion)
    end
end
