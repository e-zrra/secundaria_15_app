class EstadoSaludsController < ApplicationController
  
  include ApplicationHelper
  before_action :search, only: [:show, :edit, :new]
  before_action :set_estado_salud, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /estado_saluds
  # GET /estado_saluds.json
  def index
    if params['search']
      search  = params['search']
      @estado_saluds = EstadoSalud.where("nombre ILIKE ? OR descripcion ILIKE ?","%#{search}%", "%#{search}%").paginate(:page => params[:page], :per_page => 8)
    else
      @estado_saluds = EstadoSalud.paginate(:page => params[:page], :per_page => 10)
    end
  end

  # GET /estado_saluds/1
  # GET /estado_saluds/1.json
  def show
  end

  # GET /estado_saluds/new
  def new
    @estado_salud = EstadoSalud.new
  end

  # GET /estado_saluds/1/edit
  def edit
  end

  # POST /estado_saluds
  # POST /estado_saluds.json
  def create
    @estado_salud = EstadoSalud.new(estado_salud_params)
    respond_to do |format|
      if @estado_salud.save
        format.html { redirect_to @estado_salud, notice: 'Estado de salud fue creado.' }
        format.json { render :show, status: :created, location: @estado_salud }
      else
        format.html { render :new }
        format.json { render json: @estado_salud.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /estado_saluds/1
  # PATCH/PUT /estado_saluds/1.json
  def update
    respond_to do |format|
      if @estado_salud.update(estado_salud_params)
        format.html { redirect_to @estado_salud, notice: 'Estado de salud fue actualizado.' }
        format.json { render :show, status: :ok, location: @estado_salud }
      else
        format.html { render :edit }
        format.json { render json: @estado_salud.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /estado_saluds/1
  # DELETE /estado_saluds/1.json
  def destroy
    @estado_salud.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Estado de salud fue eliminado.' }
      format.json { head :no_content }
    end
  end #estado_saluds_url

  private

    def search
      redirect_to_search_helper(estado_saluds_url)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_estado_salud
      @estado_salud = EstadoSalud.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def estado_salud_params
      params.require(:estado_salud).permit(:nombre, :descripcion)
    end
end
