class GradoGruposController < ApplicationController
  
  include ApplicationHelper
  before_action :authenticate_user!
  before_action :search, only: [:show, :edit, :new]
  before_action :set_grado_grupo, only: [:show, :edit, :update, :destroy]

  
  # GET /grado_grupos
  # GET /grado_grupos.json
  def index
    if params['search']
      search  = params['search']
      @grado_grupos = GradoGrupo.where("grado ILIKE ? OR grupo ILIKE ?","%#{search}%", "%#{search}%").paginate(:page => params[:page], :per_page => 8)
    else
      @grado_grupos = GradoGrupo.paginate(:page => params[:page], :per_page => 10)
    end
  end

  # GET /grado_grupos/1
  # GET /grado_grupos/1.json
  def show
  end

  # GET /grado_grupos/new
  def new
    @grado_grupo = GradoGrupo.new
  end

  # GET /grado_grupos/1/edit
  def edit
  end

  # POST /grado_grupos
  # POST /grado_grupos.json
  def create
    @grado_grupo = GradoGrupo.new(grado_grupo_params)
    respond_to do |format|
      if @grado_grupo.save
        format.html { redirect_to @grado_grupo, notice: 'Grado  y grupo fue creado.' }
        format.json { render :show, status: :created, location: @grado_grupo }
      else
        format.html { render :new }
        format.json { render json: @grado_grupo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /grado_grupos/1
  # PATCH/PUT /grado_grupos/1.json
  def update
    respond_to do |format|
      if @grado_grupo.update(grado_grupo_params)
        format.html { redirect_to @grado_grupo, notice: 'Grado y grupo fue actualizado.' }
        format.json { render :show, status: :ok, location: @grado_grupo }
      else
        format.html { render :edit }
        format.json { render json: @grado_grupo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /grado_grupos/1
  # DELETE /grado_grupos/1.json
  def destroy
    @grado_grupo.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Grado y grupo fue eliminado.' }
      format.json { head :no_content }
    end
  end #grado_grupos_url

  private

    def search
      redirect_to_search_helper(grado_grupos_url)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_grado_grupo
      @grado_grupo = GradoGrupo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def grado_grupo_params
      params.require(:grado_grupo).permit(:grado, :grupo)
    end
end
