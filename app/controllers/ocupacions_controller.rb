class OcupacionsController < ApplicationController
  
  include ApplicationHelper
  before_action :authenticate_user!
  before_action :search, only: [:show, :edit, :new]
  before_action :set_ocupacion, only: [:show, :edit, :update, :destroy]
  # GET /ocupacions
  # GET /ocupacions.json
  def index
    if params['search']
      search  = params['search']
      @ocupacions = Ocupacion.where("nombre ILIKE ? OR descripcion ILIKE ?","%#{search}%", "%#{search}%").paginate(:page => params[:page], :per_page => 8)
    else
      @ocupacions = Ocupacion.paginate(:page => params[:page], :per_page => 10)
    end
  end

  # GET /ocupacions/1
  # GET /ocupacions/1.json
  def show
  end

  # GET /ocupacions/new
  def new
    @ocupacion = Ocupacion.new
  end

  # GET /ocupacions/1/edit
  def edit
  end

  # POST /ocupacions
  # POST /ocupacions.json
  def create
    @ocupacion = Ocupacion.new(ocupacion_params)
    respond_to do |format|
      if @ocupacion.save
        format.html { redirect_to @ocupacion, notice: 'Ocupacion fue creada.' }
        format.json { render :show, status: :created, location: @ocupacion }
      else
        format.html { render :new }
        format.json { render json: @ocupacion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ocupacions/1
  # PATCH/PUT /ocupacions/1.json
  def update
    respond_to do |format|
      if @ocupacion.update(ocupacion_params)
        format.html { redirect_to @ocupacion, notice: 'Ocupacion fue actualizada.' }
        format.json { render :show, status: :ok, location: @ocupacion }
      else
        format.html { render :edit }
        format.json { render json: @ocupacion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ocupacions/1
  # DELETE /ocupacions/1.json
  def destroy
    @ocupacion.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Ocupacion fue eliminada.' }
      format.json { head :no_content }
    end
  end #ocupacions_url

  private

    def search
      redirect_to_search_helper(ocupacions_url)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_ocupacion
      @ocupacion = Ocupacion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ocupacion_params
      params.require(:ocupacion).permit(:nombre, :descripcion)
    end
end
