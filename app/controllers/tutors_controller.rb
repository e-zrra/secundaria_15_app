class TutorsController < ApplicationController

  include ApplicationHelper, AlumnosHelper
  before_action :search, only: [:show, :edit, :new]
  before_action :set_alumno_tutor, only: [:show, :edit, :update, :destroy]
  Root_tutor_directory = Rails.root + 'app/assets/images/tutores'
  before_action :authenticate_user!

  # GET /tutors
  # GET /alumno_tutors.json
  def index
    if params['search']
      search  = params['search']
      @tutors = Tutor.where("nombre_completo ILIKE ? OR telefono ILIKE ?","%#{search}%", "%#{search}%").paginate(:page => params[:page], :per_page => 8)
    else
      @tutors = Tutor.paginate(:page => params[:page], :per_page => 10)
    end
  end

  # GET /tutors/1
  # GET /tutors/1.json
  def show
  end

  # GET /tutors/new
  def new
    #@alumno_id = params[:alumno_id]
    #@alumno = Alumno.find(@alumno_id)
    @tutor = Tutor.new
  end

  # GET /tutors/1/edit
  def edit
    #@alumno_id = params[:alumno_id]
    #@alumno = Alumno.find(@alumno_id)
  end

  # POST /tutors
  # POST /tutors.json
  def create
    @tutor = Tutor.new(tutor_params)
    if params[:file]
      @tutor.fotografia = upload_file_helper(params[:file], Root_tutor_directory)
    end
    @tutor.nombre_completo = get_full_name

    respond_to do |format|
      if @tutor.save
        format.html { redirect_to @tutor, notice: 'Tutor fue creado.' }
        format.json { render :show, status: :created, location: @tutor }
      else
        format.html { render :new }
        format.json { render json: @tutor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tutors/1
  # PATCH/PUT /tutors/1.json
  def update
    if params[:file]
      @tutor.fotografia = upload_file_helper(params[:file], Root_tutor_directory)
    end
    @tutor.nombre_completo = get_full_name
    respond_to do |format|
      if @tutor.update(tutor_params)
        format.html { redirect_to @tutor , notice: 'Tutor fue actualizado.' }
        format.json { render :show, status: :ok, location: @tutor }
      else
        format.html { render :edit }
        format.json { render json: @tutor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tutors/1
  # DELETE /tutors/1.json
  def destroy
    if @tutor.fotografia
      remove_file_helper(@tutor.fotografia, Root_tutor_directory)
    end
    #AlumnoTutor.where(tutor_id: @tutor.id).destroy_all

    @tutor.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Tutor fue eliminado.' }
      format.json { head :no_content }
    end
  end

  # GET /tutors/search_alumno
  def search_tutor
    nombre = params[:nombre] == nil ? '' : params[:nombre]
    apellido_paterno = params[:apellido_paterno] ==   nil ? '' : params[:apellido_paterno]
    @tutores = Tutor.where("nombre ILIKE '%#{nombre}%'").where("apellido_paterno ILIKE '%#{apellido_paterno}%'")
    render json: @tutores
  end 

  private

    def search
      redirect_to_search_helper(tutors_url)
    end

    def get_full_name
      return "#{params[:tutor][:nombre]} #{params[:tutor][:apellido_paterno]} #{params[:tutor][:apellido_materno]}"
    end
    
    # Use callbacks to share common setup or constraints between actions.
    def set_alumno_tutor
      @tutor = Tutor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tutor_params
      params.require(:tutor).permit(:nombre, :apellido_paterno, :apellido_materno, :lugar_nacimiento, :fecha_nacimiento, :parentesco_id, :sexo, :escolaridad_id, :ocupacion_id, :horario_inicio, :horario_finaliza, :domicilio, :telefono, :celular, :nota, :fotografia)
    end
end
