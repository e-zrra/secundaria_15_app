class UsersController < ApplicationController
  
  include ApplicationHelper
  before_action :authenticate_user!
  before_action :search, only: [:show, :edit, :new]
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  
  # GET /users
  # GET /users.json
  def index
    if params['search']
      search  = params['search']
      @users = User.where("full_name ILIKE ? OR email ILIKE ?", "%#{search}%", "%#{search}%").where.not(id: 1000000).paginate(:page => params[:page], :per_page => 8)
    else
      @users = User.where.not(id: 1000000).where.not(id: current_user.id).paginate(:page => params[:page], :per_page => 8)
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.full_name = get_full_name()
    respond_to do |format|
      if @user.save
        #@user.add_role "admin"
        format.html { redirect_to @user, notice: 'Usuario fue creado.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end  
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if user_params[:password].blank?
      params[:user].delete :password
      params[:user].delete :password_confirmation
    end
    @user.full_name = get_full_name
    respond_to do |format|
      if @user.update(user_params)
        if URI(request.referer).path == '/setting'
          format.html { redirect_to '/setting', notice: 'Perfil actualizado.' }
          format.json { render :show, status: :ok, location: @user }
        end
        format.html { redirect_to @user, notice: 'Usuario fue actualizado.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Usuario fue eliminado.' }
      format.json { head :no_content }
    end
  end #users_url

  def profile
    @user = User.find(current_user.id)
  end

  private

    def get_full_name
      return "#{params[:user][:first_name]} #{params[:user][:last_name]}"
    end

    def search
      redirect_to_search_helper(users_url)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email, :first_name, :last_name, :password, :password_confirmation, :admin)
    end
end
