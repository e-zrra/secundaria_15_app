class WelcomeController < ApplicationController
  before_action :authenticate_user!
  
  def index
  	@alumnos 				= Alumno.limit(8)
  	@alumno_total 			= Alumno.count
  	@usuarios_total 		= User.count
  	@grados_grupos_total 	= GradoGrupo.count
  	@reportes_total 		= AlumnoComportamiento.count
  end
  
end
