module AlumnosHelper
	def get_age (birthday)
		year = Date.today.year
		return year.to_i  - birthday.strftime('%Y').to_i
	end
	
	def get_matricula
	    year = Time.now.strftime("%y")
	    alumnos = Alumno.all
	    unless alumnos.empty?
	      last_alumno = alumnos.last.matricula
	      last_alumno_year = last_alumno[0, 2]
	      if last_alumno_year == year
	        number = last_alumno.to_i + 1
	      else
	        number = year + '00001'
	      end
	    else
	      number = year + '00001'
	    end
	    return number.to_s
	end
end
