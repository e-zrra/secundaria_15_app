module ApplicationHelper

	def upload_file_helper (file, root)
	  filename 	 = file.original_filename;
	  random 		 = rand(25)
	  filename 	 = "#{random.to_s}-#{filename}" 
    path 		   = File.join(root, filename);
    File.open(path, "wb") { |f| f.write(file.read) };
	  return filename
	end

  def remove_file_helper (name, root)
    File.delete("#{root}/#{name}")
  end

	def redirect_to_search_helper(url)
    	if params['search']
      		return redirect_to "#{url}?search=#{params['search']}"
    	end
  	end

  	def get_controller_herlper
  		Rails.application.routes.router.recognize(request) do |route, matches, param|
		  return matches[:controller]
		end
  	end

  	def active_option_nav(path)
  		hash = { 
  			'alumnos' 		=> 'alumnos',
  			'docentes' 		=> 'docentes',
  			'users' 		=> 'users',
  			'grado_grupos' 	=> 'grado_grupos',
  			'ciclo_escolars'=> 'ciclo_escolars',
  			'turnos' 		=> 'turnos',
  			'ocupacions' 	=> 'ocupacions',
  			'escolaridads' 	=> 'escolaridads',
  			'parentescos' 	=> 'parentescos',
  			'estado_saluds' => 'estado_saluds',
  			#'alumnos' 		=> 'alumno_comportamientos',
  			#'alumnos' 		=> 'comportamientos'
  		}

  		route = get_controller_herlper

	    hash.each do |key, value|
	    	if key == path and value == route 
	    		#return 'active'
	    	end
	    end
  	end

end
