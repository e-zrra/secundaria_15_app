class Alumno < ActiveRecord::Base
  resourcify
  
  belongs_to 	:estado_salud
  belongs_to 	:grado_grupo
  has_many 		:alumno_tutor, dependent: :nullify
  has_many 		:alumno_comportamiento, dependent: :nullify
  has_many 		:alumno_hermano, dependent: :nullify
  belongs_to 	:ciclo_escolar
  has_many 		:alumno_ciclo, dependent: :nullify
  belongs_to	:turno
end