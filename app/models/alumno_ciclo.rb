class AlumnoCiclo < ActiveRecord::Base
  belongs_to :alumno
  belongs_to :ciclo_escolar
  has_many :alumno_tutor, :class_name => 'AlumnoTutor'
end
