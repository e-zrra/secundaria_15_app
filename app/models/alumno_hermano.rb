class AlumnoHermano < ActiveRecord::Base
  belongs_to :alumno
  belongs_to :hermano, :class_name => "Alumno"
end
