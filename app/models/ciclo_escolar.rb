class CicloEscolar < ActiveRecord::Base
	has_many :alumno, dependent: :nullify
	has_many :alumno_ciclo, dependent: :nullify
end
