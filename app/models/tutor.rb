class Tutor < ActiveRecord::Base
  	belongs_to :alumno
  	belongs_to :escolaridad
  	belongs_to :ocupacion
  	belongs_to :parentesco
	has_many   :alumno_tutor, dependent: :nullify
end
