json.array!(@alumno_comportamientos) do |alumno_comportamiento|
  json.extract! alumno_comportamiento, :id, :alumno_id, :comportamiento_id, :fecha, :nota, :fotografia
  json.url alumno_comportamiento_url(alumno_comportamiento, format: :json)
end
