json.array!(@alumno_hermanos) do |alumno_hermano|
  json.extract! alumno_hermano, :id, :alumno_id, :hermano_id
  json.url alumno_hermano_url(alumno_hermano, format: :json)
end
