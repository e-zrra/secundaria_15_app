json.array!(@alumno_tutors) do |alumno_tutor|
  json.extract! alumno_tutor, :id, :alumno_id, :tutor_id
  json.url alumno_tutor_url(alumno_tutor, format: :json)
end
