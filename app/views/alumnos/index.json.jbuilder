json.array!(@alumnos) do |alumno|
  json.extract! alumno, :id, :matricula, :nombre, :apellido_paterno, :apellido_materno, :fecha_nacimiento, :lugar_nacimiento, :curp, :escuela_procedencia, :promedio, :toma_medicamento, :antecedentes_medicos, :estado_salud_id, :domicilio, :telefono, :celular, :nota, :grado_grupo_id, :fotografia
  json.url alumno_url(alumno, format: :json)
end
