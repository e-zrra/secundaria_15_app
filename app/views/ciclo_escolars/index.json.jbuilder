json.array!(@ciclo_escolars) do |ciclo_escolar|
  json.extract! ciclo_escolar, :id, :nombre, :descripcion, :inicio_fecha, :finaliza_fecha
  json.url ciclo_escolar_url(ciclo_escolar, format: :json)
end
