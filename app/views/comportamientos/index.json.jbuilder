json.array!(@comportamientos) do |comportamiento|
  json.extract! comportamiento, :id, :nombre, :descripcion
  json.url comportamiento_url(comportamiento, format: :json)
end
