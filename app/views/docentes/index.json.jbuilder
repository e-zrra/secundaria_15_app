json.array!(@docentes) do |docente|
  json.extract! docente, :id, :nombre, :apellido_paterno, :apellido_materno, :fecha_nacimiento, :lugar_nacimiento, :telefono, :fotografia
  json.url docente_url(docente, format: :json)
end
