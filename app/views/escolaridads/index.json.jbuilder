json.array!(@escolaridads) do |escolaridad|
  json.extract! escolaridad, :id, :nombre, :descripcion
  json.url escolaridad_url(escolaridad, format: :json)
end
