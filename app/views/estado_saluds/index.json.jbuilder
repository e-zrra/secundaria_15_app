json.array!(@estado_saluds) do |estado_salud|
  json.extract! estado_salud, :id, :nombre, :descripcion
  json.url estado_salud_url(estado_salud, format: :json)
end
