json.array!(@grado_grupos) do |grado_grupo|
  json.extract! grado_grupo, :id, :grado, :grupo
  json.url grado_grupo_url(grado_grupo, format: :json)
end
