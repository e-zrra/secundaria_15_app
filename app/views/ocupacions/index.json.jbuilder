json.array!(@ocupacions) do |ocupacion|
  json.extract! ocupacion, :id, :nombre, :descripcion
  json.url ocupacion_url(ocupacion, format: :json)
end
