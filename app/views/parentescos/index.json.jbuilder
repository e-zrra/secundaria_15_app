json.array!(@parentescos) do |parentesco|
  json.extract! parentesco, :id, :nombre, :descripcion
  json.url parentesco_url(parentesco, format: :json)
end
