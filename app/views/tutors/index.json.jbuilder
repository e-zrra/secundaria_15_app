json.array!(@alumno_tutors) do |alumno_tutor|
  json.extract! alumno_tutor, :id, :alumno_id, :nombre, :apellido_paterno, :apellido_materno, :lugar_nacimiento, :fecha_nacimiento, :parentesco, :sexo, :escolaridad_id, :ocupacion_id, :horario_inicio, :horario_finaliza, :domicilio, :telefono, :celuar, :nota, :fotografia
  json.url alumno_tutor_url(alumno_tutor, format: :json)
end
