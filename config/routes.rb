Rails.application.routes.draw do
  resources :docentes
  resources :turnos
  resources :estado_saluds
  resources :comportamientos
  resources :ciclo_escolars
  resources :alumno_ciclos
  resources :ocupacions
  resources :escolaridads
  resources :parentescos
  resources :tutors
  devise_for :users, :controllers => { :registrations => "users" }
  resources  :users
  resources :grado_grupos

  resources :alumnos do
    get 'alumnos_by_group_grado', :on => :collection
    get 'increase_by_grupo_grado', :on => :collection
    get 'increase_by_ciclo_escolar', :on => :collection
  end

  resources :alumno_tutors do
    get 'search_tutor', :on => :collection
  end

  resources :alumno_hermanos do
    get 'search_alumno', :on => :collection
  end

  resources :alumno_comportamientos do
    get 'search_alumno', :on => :collection
  end

  root  'welcome#index'
  get   'setting' => 'users#profile', as: :user_profile
  match 'setting' => 'users#update', via: [:put, :patch]

end
