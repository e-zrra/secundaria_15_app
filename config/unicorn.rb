root = "/opt/www/secundaria_15_app/current"
working_directory root
pid "#{root}/tmp/pids/unicorn.pid"
stderr_path "#{root}/log/unicorn.log"
stdout_path "#{root}/log/unicorn.log"

listen "/tmp/unicorn.secundaria_15_app.sock"
worker_processes 1
timeout 30