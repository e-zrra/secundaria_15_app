class CreateEscolaridads < ActiveRecord::Migration
  def change
    create_table :escolaridads do |t|
      t.string :nombre
      t.text :descripcion

      t.timestamps null: false
    end
  end
end
