class CreateOcupacions < ActiveRecord::Migration
  def change
    create_table :ocupacions do |t|
      t.string :nombre
      t.text :descripcion

      t.timestamps null: false
    end
  end
end
