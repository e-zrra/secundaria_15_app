class CreateGradoGrupos < ActiveRecord::Migration
  def change
    create_table :grado_grupos do |t|
      t.string :grado
      t.string :grupo

      t.timestamps null: false
    end
  end
end
