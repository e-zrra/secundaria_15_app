class CreateAlumnos < ActiveRecord::Migration
  def change
    create_table :alumnos do |t|
      t.string :matricula
      t.string :nombre
      t.string :apellido_paterno
      t.string :apellido_materno
      t.string :nombre_completo
      t.date :fecha_nacimiento
      t.text :lugar_nacimiento
      t.string :curp
      t.string :escuela_procedencia
      t.float :promedio
      t.boolean :toma_medicamento
      t.text :antecedentes_medicos
      t.references :estado_salud, index: true, foreign_key: true
      t.references :turno, index: true, foreign_key: true
      t.text :domicilio
      t.string :telefono
      t.string :celular
      t.text :nota
      t.references :grado_grupo, index: true, foreign_key: true
      t.string :fotografia

      t.timestamps null: false
    end
  end
end
