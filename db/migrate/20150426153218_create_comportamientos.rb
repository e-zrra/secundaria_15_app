class CreateComportamientos < ActiveRecord::Migration
  def change
    create_table :comportamientos do |t|
      t.string :nombre
      t.text :descripcion

      t.timestamps null: false
    end
  end
end
