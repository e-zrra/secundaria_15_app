class CreateAlumnoComportamientos < ActiveRecord::Migration
  def change
    create_table :alumno_comportamientos do |t|
      t.references :alumno, index: true, foreign_key: true
      t.references :comportamiento, index: true, foreign_key: true
      t.date :fecha
      t.text :nota
      t.string :fotografia

      t.timestamps null: false
    end
  end
end
