class CreateTutors < ActiveRecord::Migration
  def change
    create_table :tutors do |t|
      t.string :nombre
      t.string :apellido_paterno
      t.string :apellido_materno
      t.string :nombre_completo
      t.text :lugar_nacimiento
      t.date :fecha_nacimiento
      t.references :parentesco, index: true, foreign_key: true
      t.integer :sexo
      t.references :escolaridad, index: true, foreign_key: true
      t.references :ocupacion, index: true, foreign_key: true
      t.time :horario_inicio
      t.time :horario_finaliza
      t.text :domicilio
      t.string :telefono
      t.string :celular
      t.text :nota
      t.string :fotografia

      t.timestamps null: false
    end
  end
end
