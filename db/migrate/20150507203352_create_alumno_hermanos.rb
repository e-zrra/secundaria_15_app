class CreateAlumnoHermanos < ActiveRecord::Migration
  def change
    create_table :alumno_hermanos do |t|
      t.references :alumno, index: true, foreign_key: true
      #t.references :hermano, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
