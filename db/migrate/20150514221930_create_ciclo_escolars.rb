class CreateCicloEscolars < ActiveRecord::Migration
  def change
    create_table :ciclo_escolars do |t|
      t.string :nombre
      t.text :descripcion
      t.date :inicio_fecha
      t.date :finaliza_fecha

      t.timestamps null: false
    end
  end
end
