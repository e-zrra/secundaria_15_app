class CreateAlumnoCiclos < ActiveRecord::Migration
  def change
    create_table :alumno_ciclos do |t|
      t.references :alumno, index: true, foreign_key: true
      t.references :ciclo_escolar, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
