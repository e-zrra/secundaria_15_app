class AddCicloEscolarToAlumno < ActiveRecord::Migration
  def change
    add_reference :alumnos, :ciclo_escolar, index: true, foreign_key: true
  end
end
