class CreateDocentes < ActiveRecord::Migration
  def change
    create_table :docentes do |t|
      t.string :nombre
      t.string :apellido_paterno
      t.string :apellido_materno
      t.string :nombre_completo
      t.date   :fecha_nacimiento
      t.text   :lugar_nacimiento
      t.text   :domicilio
      t.string :telefono
      t.string :celular
      t.string :fotografia

      t.timestamps null: false
    end
  end
end
