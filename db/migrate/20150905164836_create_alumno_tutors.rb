class CreateAlumnoTutors < ActiveRecord::Migration
  def change
    create_table :alumno_tutors do |t|
      t.references :alumno, index: true, foreign_key: true
      t.references :tutor, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
