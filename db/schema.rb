# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150905164836) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "alumno_ciclos", force: :cascade do |t|
    t.integer  "alumno_id"
    t.integer  "ciclo_escolar_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "alumno_ciclos", ["alumno_id"], name: "index_alumno_ciclos_on_alumno_id", using: :btree
  add_index "alumno_ciclos", ["ciclo_escolar_id"], name: "index_alumno_ciclos_on_ciclo_escolar_id", using: :btree

  create_table "alumno_comportamientos", force: :cascade do |t|
    t.integer  "alumno_id"
    t.integer  "comportamiento_id"
    t.date     "fecha"
    t.text     "nota"
    t.string   "fotografia"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "alumno_comportamientos", ["alumno_id"], name: "index_alumno_comportamientos_on_alumno_id", using: :btree
  add_index "alumno_comportamientos", ["comportamiento_id"], name: "index_alumno_comportamientos_on_comportamiento_id", using: :btree

  create_table "alumno_hermanos", force: :cascade do |t|
    t.integer  "alumno_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "hermano_id"
  end

  add_index "alumno_hermanos", ["alumno_id"], name: "index_alumno_hermanos_on_alumno_id", using: :btree

  create_table "alumno_tutors", force: :cascade do |t|
    t.integer  "alumno_id"
    t.integer  "tutor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "alumno_tutors", ["alumno_id"], name: "index_alumno_tutors_on_alumno_id", using: :btree
  add_index "alumno_tutors", ["tutor_id"], name: "index_alumno_tutors_on_tutor_id", using: :btree

  create_table "alumnos", force: :cascade do |t|
    t.string   "matricula"
    t.string   "nombre"
    t.string   "apellido_paterno"
    t.string   "apellido_materno"
    t.string   "nombre_completo"
    t.date     "fecha_nacimiento"
    t.text     "lugar_nacimiento"
    t.string   "curp"
    t.string   "escuela_procedencia"
    t.float    "promedio"
    t.boolean  "toma_medicamento"
    t.text     "antecedentes_medicos"
    t.integer  "estado_salud_id"
    t.integer  "turno_id"
    t.text     "domicilio"
    t.string   "telefono"
    t.string   "celular"
    t.text     "nota"
    t.integer  "grado_grupo_id"
    t.string   "fotografia"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "ciclo_escolar_id"
  end

  add_index "alumnos", ["ciclo_escolar_id"], name: "index_alumnos_on_ciclo_escolar_id", using: :btree
  add_index "alumnos", ["estado_salud_id"], name: "index_alumnos_on_estado_salud_id", using: :btree
  add_index "alumnos", ["grado_grupo_id"], name: "index_alumnos_on_grado_grupo_id", using: :btree
  add_index "alumnos", ["turno_id"], name: "index_alumnos_on_turno_id", using: :btree

  create_table "ciclo_escolars", force: :cascade do |t|
    t.string   "nombre"
    t.text     "descripcion"
    t.date     "inicio_fecha"
    t.date     "finaliza_fecha"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "comportamientos", force: :cascade do |t|
    t.string   "nombre"
    t.text     "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "codigo"
  end

  create_table "docentes", force: :cascade do |t|
    t.string   "nombre"
    t.string   "apellido_paterno"
    t.string   "apellido_materno"
    t.string   "nombre_completo"
    t.date     "fecha_nacimiento"
    t.text     "lugar_nacimiento"
    t.text     "domicilio"
    t.string   "telefono"
    t.string   "celular"
    t.string   "fotografia"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "escolaridads", force: :cascade do |t|
    t.string   "nombre"
    t.text     "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "estado_saluds", force: :cascade do |t|
    t.string   "nombre"
    t.text     "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "grado_grupos", force: :cascade do |t|
    t.string   "grado"
    t.string   "grupo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ocupacions", force: :cascade do |t|
    t.string   "nombre"
    t.text     "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "parentescos", force: :cascade do |t|
    t.string   "nombre"
    t.text     "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "profiles", force: :cascade do |t|
    t.integer "user_id"
    t.string  "fecha_nacimiento"
  end

  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "turnos", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tutors", force: :cascade do |t|
    t.string   "nombre"
    t.string   "apellido_paterno"
    t.string   "apellido_materno"
    t.string   "nombre_completo"
    t.text     "lugar_nacimiento"
    t.date     "fecha_nacimiento"
    t.integer  "parentesco_id"
    t.integer  "sexo"
    t.integer  "escolaridad_id"
    t.integer  "ocupacion_id"
    t.time     "horario_inicio"
    t.time     "horario_finaliza"
    t.text     "domicilio"
    t.string   "telefono"
    t.string   "celular"
    t.text     "nota"
    t.string   "fotografia"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "tutors", ["escolaridad_id"], name: "index_tutors_on_escolaridad_id", using: :btree
  add_index "tutors", ["ocupacion_id"], name: "index_tutors_on_ocupacion_id", using: :btree
  add_index "tutors", ["parentesco_id"], name: "index_tutors_on_parentesco_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "full_name"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  add_foreign_key "alumno_ciclos", "alumnos"
  add_foreign_key "alumno_ciclos", "ciclo_escolars"
  add_foreign_key "alumno_comportamientos", "alumnos"
  add_foreign_key "alumno_comportamientos", "comportamientos"
  add_foreign_key "alumno_hermanos", "alumnos"
  add_foreign_key "alumno_tutors", "alumnos"
  add_foreign_key "alumno_tutors", "tutors"
  add_foreign_key "alumnos", "ciclo_escolars"
  add_foreign_key "alumnos", "estado_saluds"
  add_foreign_key "alumnos", "grado_grupos"
  add_foreign_key "alumnos", "turnos"
  add_foreign_key "profiles", "users"
  add_foreign_key "tutors", "escolaridads"
  add_foreign_key "tutors", "ocupacions"
  add_foreign_key "tutors", "parentescos"
end
