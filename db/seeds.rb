#encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Escolaridad.destroy_all

Escolaridad.create([
	{ nombre: 'Primaria', descripcion: 'Primaria' },
	{ nombre: 'Secundaria', descripcion: 'Secundaria' },
	{ nombre: 'Bachillerato', descripcion: 'Bachillerato' },
	{ nombre: 'Universidad', descripcion: 'Universidad' },
	{ nombre: 'Maestria', descripcion: 'Maestria' },
	{ nombre: 'Doctorado', descripcion: 'Doctorado' },
]);

Parentesco.destroy_all

Parentesco.create([
	{ nombre: 'Padre', descripcion: 'Padre' },
	{ nombre: 'Madre', descripcion: 'Madre' },
	{ nombre: 'Abuelo', descripcion: 'Abuelo' },
	{ nombre: 'Hermano', descripcion: 'Hermano' },
	{ nombre: 'Primo', descripcion: 'Primo' },
	{ nombre: 'Tío', descripcion: 'Tío' },
]);

EstadoSalud.destroy_all
EstadoSalud.create([
	{ nombre: 'Buena', descripcion: 'Salud buena, saludable' },
])

Ocupacion.destroy_all
Ocupacion.create([
	{ nombre: 'Obrero', descripcion: 'Obrero' },
	{ nombre: 'Ama de casa', descripcion: 'Ama de casa' },
	{ nombre: 'Estudiante', descripcion: 'Estudiante' },
	{ nombre: 'Profesionista', descripcion: 'Profesionista' },
	{ nombre: 'Ocupación en salud', descripcion: 'Ocupación en salud' },
	{ nombre: 'Ocupación en educación', descripcion: 'Ocupación en educación' },
	{ nombre: 'Ventas y servicios', descripcion: 'Ventas y servicios' },
	{ nombre: 'Ocupación en transporte', descripcion: 'Ocupación en transporte' },
	{ nombre: 'Ocupación en finanzas y gerencia', descripcion: 'Ocupación en finanzas y gerencia' },
])

Comportamiento.destroy_all
Comportamiento.create([
	{ codigo: 'AF', nombre: 'Abuso Físico', descripcion: 'Abuso físico: manotazos, golpes, empujones, coscorrones, lanzar objetos.' },
	{ codigo: 'ASF', nombre: 'Abuso Sexual Físico', descripcion: 'Tocar partes íntimas, repegones, manoseo, etc.' },
	{ codigo: 'ASO', nombre: 'Abuso Sexual Visual', descripcion: 'Mostrar videos, fotos, relatos pornográficos; exponer sus partes íntimas a otros.' },
	{ codigo: 'ASV', nombre: 'Abuso Sexual Verbal', descripcion: 'Insinuaciones, comentarios sexuales despectivos o fuera de lugar.' },
	{ codigo: 'B', nombre: 'Bullying', descripcion: 'Conductas inapropiadas que afectan el bien estar de otro compañero.' },
	{ codigo: 'C', nombre: 'Citatorio', descripcion: 'Citar a los padres.' },
	{ codigo: 'D', nombre: 'Distracción', descripcion: 'Distrae a sus compañeros, se pone a jugar, escucha música, etc.' },
	{ codigo: 'G', nombre: 'Grafitear ', descripcion: 'Rallar mesabancos o infraestructura escolar con plumones, lápiz, pintura, etc.' },
	{ codigo: 'LO', nombre: 'Lenguaje Obsceno', descripcion: 'Malas palabras dirigidas o no a personas' },
	{ codigo: 'M', nombre: 'Material', descripcion: 'No trae material escolar adecuado.' },
	{ codigo: 'NA', nombre: 'No Actividades', descripcion: 'No hace tareas.' },
	{ codigo: 'NT', nombre: 'No trabaja', descripcion: 'No hace trabajos en clase ni participa.' },
	{ codigo: 'PL', nombre: 'Platicador', descripcion: 'Se la pasa platicando y no atiende sus trabajos.' },
	{ codigo: 'R', nombre: 'Reporte', descripcion: 'Reporte por conducta indeseable, violar el reglamento o las normas escolares.' },
	{ codigo: 'S', nombre: 'Se salió del salón', descripcion: 'Salió de la clase y ya no regresó.' },
	{ codigo: 'SUS', nombre: 'Suspensión', descripcion: 'Suspendido de clase.' },
	{ codigo: 'U', nombre: 'Sin uniforme', descripcion: 'No llevar el uniforme completo.' },
	{ codigo: 'X', nombre: 'Falta', descripcion: 'No asistió a clase.' },
	{ codigo: 'Ot', nombre: 'Otros', descripcion: 'Cualquier situación fuera del listado que amerite anotarse. ' },
])

User.destroy_all

User.create(
	:id 					=> 1000000,
	:first_name 			=> 'Nombre',
	:last_name 				=> 'Apellido',
	:full_name 				=> 'Nombre Apellido',
  	:email                	=> 'admin@admin.com', 
  	:password   			=> 'password',
  	:password_confirmation  => 'password',
  	:admin 					=> true
)

GradoGrupo.destroy_all

GradoGrupo.create(
	:id => 1,
	:grado => '1',
	:grupo => 'A'
)

GradoGrupo.create(
	:id => 2,
	:grado => '2',
	:grupo => 'A'
)

GradoGrupo.create(
	:id => 3,
	:grado => '3',
	:grupo => 'A'
)

GradoGrupo.create(
	:id => 4,
	:grado => '4',
	:grupo => 'A'
)