require 'test_helper'

class AlumnoComportamientosControllerTest < ActionController::TestCase
  setup do
    @alumno_comportamiento = alumno_comportamientos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:alumno_comportamientos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create alumno_comportamiento" do
    assert_difference('AlumnoComportamiento.count') do
      post :create, alumno_comportamiento: { alumno_id: @alumno_comportamiento.alumno_id, comportamiento_id: @alumno_comportamiento.comportamiento_id, fecha: @alumno_comportamiento.fecha, fotografia: @alumno_comportamiento.fotografia, nota: @alumno_comportamiento.nota }
    end

    assert_redirected_to alumno_comportamiento_path(assigns(:alumno_comportamiento))
  end

  test "should show alumno_comportamiento" do
    get :show, id: @alumno_comportamiento
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @alumno_comportamiento
    assert_response :success
  end

  test "should update alumno_comportamiento" do
    patch :update, id: @alumno_comportamiento, alumno_comportamiento: { alumno_id: @alumno_comportamiento.alumno_id, comportamiento_id: @alumno_comportamiento.comportamiento_id, fecha: @alumno_comportamiento.fecha, fotografia: @alumno_comportamiento.fotografia, nota: @alumno_comportamiento.nota }
    assert_redirected_to alumno_comportamiento_path(assigns(:alumno_comportamiento))
  end

  test "should destroy alumno_comportamiento" do
    assert_difference('AlumnoComportamiento.count', -1) do
      delete :destroy, id: @alumno_comportamiento
    end

    assert_redirected_to alumno_comportamientos_path
  end
end
