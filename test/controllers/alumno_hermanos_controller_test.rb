require 'test_helper'

class AlumnoHermanosControllerTest < ActionController::TestCase
  setup do
    @alumno_hermano = alumno_hermanos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:alumno_hermanos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create alumno_hermano" do
    assert_difference('AlumnoHermano.count') do
      post :create, alumno_hermano: { alumno_id: @alumno_hermano.alumno_id, hermano_id: @alumno_hermano.hermano_id }
    end

    assert_redirected_to alumno_hermano_path(assigns(:alumno_hermano))
  end

  test "should show alumno_hermano" do
    get :show, id: @alumno_hermano
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @alumno_hermano
    assert_response :success
  end

  test "should update alumno_hermano" do
    patch :update, id: @alumno_hermano, alumno_hermano: { alumno_id: @alumno_hermano.alumno_id, hermano_id: @alumno_hermano.hermano_id }
    assert_redirected_to alumno_hermano_path(assigns(:alumno_hermano))
  end

  test "should destroy alumno_hermano" do
    assert_difference('AlumnoHermano.count', -1) do
      delete :destroy, id: @alumno_hermano
    end

    assert_redirected_to alumno_hermanos_path
  end
end
