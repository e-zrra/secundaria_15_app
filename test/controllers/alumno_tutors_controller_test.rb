require 'test_helper'

class AlumnoTutorsControllerTest < ActionController::TestCase
  setup do
    @alumno_tutor = alumno_tutors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:alumno_tutors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create alumno_tutor" do
    assert_difference('AlumnoTutor.count') do
      post :create, alumno_tutor: { alumno_id: @alumno_tutor.alumno_id, tutor_id: @alumno_tutor.tutor_id }
    end

    assert_redirected_to alumno_tutor_path(assigns(:alumno_tutor))
  end

  test "should show alumno_tutor" do
    get :show, id: @alumno_tutor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @alumno_tutor
    assert_response :success
  end

  test "should update alumno_tutor" do
    patch :update, id: @alumno_tutor, alumno_tutor: { alumno_id: @alumno_tutor.alumno_id, tutor_id: @alumno_tutor.tutor_id }
    assert_redirected_to alumno_tutor_path(assigns(:alumno_tutor))
  end

  test "should destroy alumno_tutor" do
    assert_difference('AlumnoTutor.count', -1) do
      delete :destroy, id: @alumno_tutor
    end

    assert_redirected_to alumno_tutors_path
  end
end
