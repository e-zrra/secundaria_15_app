require 'test_helper'

class AlumnosControllerTest < ActionController::TestCase
  setup do
    @alumno = alumnos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:alumnos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create alumno" do
    assert_difference('Alumno.count') do
      post :create, alumno: { alumno_estado_salud_id: @alumno.alumno_estado_salud_id, antecedentes_medicos: @alumno.antecedentes_medicos, apellido_materno: @alumno.apellido_materno, apellido_paterno: @alumno.apellido_paterno, celular: @alumno.celular, curp: @alumno.curp, domicilio: @alumno.domicilio, escuela_procedencia: @alumno.escuela_procedencia, fecha_nacimiento: @alumno.fecha_nacimiento, fotografia: @alumno.fotografia, grado_grupo_id: @alumno.grado_grupo_id, lugar_nacimiento: @alumno.lugar_nacimiento, matricula: @alumno.matricula, nombre: @alumno.nombre, nota: @alumno.nota, promedio: @alumno.promedio, telefono: @alumno.telefono, toma_medicamento: @alumno.toma_medicamento }
    end

    assert_redirected_to alumno_path(assigns(:alumno))
  end

  test "should show alumno" do
    get :show, id: @alumno
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @alumno
    assert_response :success
  end

  test "should update alumno" do
    patch :update, id: @alumno, alumno: { alumno_estado_salud_id: @alumno.alumno_estado_salud_id, antecedentes_medicos: @alumno.antecedentes_medicos, apellido_materno: @alumno.apellido_materno, apellido_paterno: @alumno.apellido_paterno, celular: @alumno.celular, curp: @alumno.curp, domicilio: @alumno.domicilio, escuela_procedencia: @alumno.escuela_procedencia, fecha_nacimiento: @alumno.fecha_nacimiento, fotografia: @alumno.fotografia, grado_grupo_id: @alumno.grado_grupo_id, lugar_nacimiento: @alumno.lugar_nacimiento, matricula: @alumno.matricula, nombre: @alumno.nombre, nota: @alumno.nota, promedio: @alumno.promedio, telefono: @alumno.telefono, toma_medicamento: @alumno.toma_medicamento }
    assert_redirected_to alumno_path(assigns(:alumno))
  end

  test "should destroy alumno" do
    assert_difference('Alumno.count', -1) do
      delete :destroy, id: @alumno
    end

    assert_redirected_to alumnos_path
  end
end
