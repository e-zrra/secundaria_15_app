require 'test_helper'

class CicloEscolarsControllerTest < ActionController::TestCase
  setup do
    @ciclo_escolar = ciclo_escolars(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ciclo_escolars)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ciclo_escolar" do
    assert_difference('CicloEscolar.count') do
      post :create, ciclo_escolar: { descripcion: @ciclo_escolar.descripcion, finaliza_fecha: @ciclo_escolar.finaliza_fecha, inicio_fecha: @ciclo_escolar.inicio_fecha, nombre: @ciclo_escolar.nombre }
    end

    assert_redirected_to ciclo_escolar_path(assigns(:ciclo_escolar))
  end

  test "should show ciclo_escolar" do
    get :show, id: @ciclo_escolar
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ciclo_escolar
    assert_response :success
  end

  test "should update ciclo_escolar" do
    patch :update, id: @ciclo_escolar, ciclo_escolar: { descripcion: @ciclo_escolar.descripcion, finaliza_fecha: @ciclo_escolar.finaliza_fecha, inicio_fecha: @ciclo_escolar.inicio_fecha, nombre: @ciclo_escolar.nombre }
    assert_redirected_to ciclo_escolar_path(assigns(:ciclo_escolar))
  end

  test "should destroy ciclo_escolar" do
    assert_difference('CicloEscolar.count', -1) do
      delete :destroy, id: @ciclo_escolar
    end

    assert_redirected_to ciclo_escolars_path
  end
end
