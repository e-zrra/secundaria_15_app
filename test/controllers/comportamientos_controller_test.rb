require 'test_helper'

class ComportamientosControllerTest < ActionController::TestCase
  setup do
    @comportamiento = comportamientos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:comportamientos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create comportamiento" do
    assert_difference('Comportamiento.count') do
      post :create, comportamiento: { descripcion: @comportamiento.descripcion, nombre: @comportamiento.nombre }
    end

    assert_redirected_to comportamiento_path(assigns(:comportamiento))
  end

  test "should show comportamiento" do
    get :show, id: @comportamiento
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @comportamiento
    assert_response :success
  end

  test "should update comportamiento" do
    patch :update, id: @comportamiento, comportamiento: { descripcion: @comportamiento.descripcion, nombre: @comportamiento.nombre }
    assert_redirected_to comportamiento_path(assigns(:comportamiento))
  end

  test "should destroy comportamiento" do
    assert_difference('Comportamiento.count', -1) do
      delete :destroy, id: @comportamiento
    end

    assert_redirected_to comportamientos_path
  end
end
