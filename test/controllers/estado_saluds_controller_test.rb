require 'test_helper'

class EstadoSaludsControllerTest < ActionController::TestCase
  setup do
    @estado_salud = estado_saluds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:estado_saluds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create estado_salud" do
    assert_difference('EstadoSalud.count') do
      post :create, estado_salud: { descripcion: @estado_salud.descripcion, nombre: @estado_salud.nombre }
    end

    assert_redirected_to estado_salud_path(assigns(:estado_salud))
  end

  test "should show estado_salud" do
    get :show, id: @estado_salud
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @estado_salud
    assert_response :success
  end

  test "should update estado_salud" do
    patch :update, id: @estado_salud, estado_salud: { descripcion: @estado_salud.descripcion, nombre: @estado_salud.nombre }
    assert_redirected_to estado_salud_path(assigns(:estado_salud))
  end

  test "should destroy estado_salud" do
    assert_difference('EstadoSalud.count', -1) do
      delete :destroy, id: @estado_salud
    end

    assert_redirected_to estado_saluds_path
  end
end
