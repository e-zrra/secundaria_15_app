require 'test_helper'

class GradoGruposControllerTest < ActionController::TestCase
  setup do
    @grado_grupo = grado_grupos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:grado_grupos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create grado_grupo" do
    assert_difference('GradoGrupo.count') do
      post :create, grado_grupo: { grado: @grado_grupo.grado, grupo: @grado_grupo.grupo }
    end

    assert_redirected_to grado_grupo_path(assigns(:grado_grupo))
  end

  test "should show grado_grupo" do
    get :show, id: @grado_grupo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @grado_grupo
    assert_response :success
  end

  test "should update grado_grupo" do
    patch :update, id: @grado_grupo, grado_grupo: { grado: @grado_grupo.grado, grupo: @grado_grupo.grupo }
    assert_redirected_to grado_grupo_path(assigns(:grado_grupo))
  end

  test "should destroy grado_grupo" do
    assert_difference('GradoGrupo.count', -1) do
      delete :destroy, id: @grado_grupo
    end

    assert_redirected_to grado_grupos_path
  end
end
