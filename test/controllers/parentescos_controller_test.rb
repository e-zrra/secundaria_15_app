require 'test_helper'

class ParentescosControllerTest < ActionController::TestCase
  setup do
    @parentesco = parentescos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:parentescos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create parentesco" do
    assert_difference('Parentesco.count') do
      post :create, parentesco: { descripcion: @parentesco.descripcion, nombre: @parentesco.nombre }
    end

    assert_redirected_to parentesco_path(assigns(:parentesco))
  end

  test "should show parentesco" do
    get :show, id: @parentesco
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @parentesco
    assert_response :success
  end

  test "should update parentesco" do
    patch :update, id: @parentesco, parentesco: { descripcion: @parentesco.descripcion, nombre: @parentesco.nombre }
    assert_redirected_to parentesco_path(assigns(:parentesco))
  end

  test "should destroy parentesco" do
    assert_difference('Parentesco.count', -1) do
      delete :destroy, id: @parentesco
    end

    assert_redirected_to parentescos_path
  end
end
